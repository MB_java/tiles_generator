import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Andrei Kutsenko
 */
public class Generator {

    private int DEFAULT_TILE_WIDTH = 256;
    private int DEFAULT_TILE_HEIGHT = 256;

    private File mapImg;
    private String resizedMapImageLocation;

    private String resultDir;

    private double leftTopLat;
    private double leftTopLng;
    private double rightBottomLat;
    private double rightBottomLng;

    private int zoomLevel;

    private static int[] getTileNumber(final double lat, final double lon, final int zoom) {
        int[] result = new int[2];
        result[0] = (int) Math.floor((lon + 180) / 360 * (1 << zoom));
        result[1] = (int) Math.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1 << zoom));
        return result;
    }

    private static double toLatitude(double mercator) {
        double radians = Math.atan(Math.exp(Math.toRadians(mercator)));
        return Math.toDegrees(2 * radians) - 90;
    }


    private BufferedImage resizeImage(BufferedImage originalImage, int w, int h) {
        BufferedImage resizedImage = new BufferedImage(w, h, 2);
        Graphics2D g = resizedImage.createGraphics();
        g.setColor(Color.red);
        g.setComposite(AlphaComposite.SrcOver);
        g.drawImage(originalImage, 0, 0, w, h, null);
        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.dispose();
        return resizedImage;
    }

    public void generateTiles() throws Exception {

        double width = Math.abs(leftTopLng - rightBottomLng);
        double height = Math.abs(leftTopLat - rightBottomLat);


        System.out.println("The presented map has geographical dimensions: (" + width + "," + height + ")");
        BufferedImage image = ImageIO.read(new FileInputStream(mapImg));
        System.out.println("Rozmiar mapy z pliku: (" + image.getWidth() + "px , " + image.getHeight() + "px)");
        System.out.println(zoomLevel);
        resizeImage(resizedMapImageLocation, image, width, height);

        int[] tileCoords = getTileNumber(leftTopLat, leftTopLng, zoomLevel);

        int size[] = getSizeInTiles(leftTopLat, leftTopLng, rightBottomLat, rightBottomLng, zoomLevel);
        splitImage(resizedMapImageLocation, resultDir, size[1], size[0], zoomLevel, tileCoords[0], tileCoords[1]);
    }

    private void resizeImage(String resizedFileLocation, BufferedImage image, double width, double height) throws Exception {

        double[] resizeValues = calculateMargins(leftTopLat, leftTopLng, rightBottomLat, rightBottomLng, zoomLevel);

        double pixelPerLng = image.getWidth() / width;
        double pixelPerLat = image.getHeight() / height;
        double left = resizeValues[0] * pixelPerLng;
        double top = resizeValues[1] * pixelPerLat;
        double right = resizeValues[2] * pixelPerLng;
        double bottom = resizeValues[3] * pixelPerLat;
        int finalWidth = (int) (image.getWidth() + left + right);
        int finalHeight = (int) (image.getHeight() + top + bottom);
        int sizeArr = finalHeight * finalWidth;
        String path = null;
        if (Integer.MAX_VALUE / sizeArr < 4) {
            double cf = 0.9;
            int w;
            int h;
            while (Integer.MAX_VALUE / sizeArr < 4) {
                w = (int) (image.getWidth() * cf);
                h = (int) (image.getHeight() * cf);
                image = resizeImage(image, w, h);
                path = mapImg.getAbsolutePath().replace("source", "aa");
                ImageIO.write(image, "png", new File(path));
                image = ImageIO.read(new FileInputStream(new File(path)));
                pixelPerLng = image.getWidth() / width;
                pixelPerLat = image.getHeight() / height;
                left = resizeValues[0] * pixelPerLng;
                top = resizeValues[1] * pixelPerLat;
                right = resizeValues[2] * pixelPerLng;
                bottom = resizeValues[3] * pixelPerLat;
                finalWidth = (int) (image.getWidth() + left + right);
                finalHeight = (int) (image.getHeight() + top + bottom);
                sizeArr = finalHeight * finalWidth;
            }
            if (path != null)
                image = ImageIO.read(new FileInputStream(new File(path)));
        }
        BufferedImage result = new BufferedImage(finalWidth, finalHeight, BufferedImage.TYPE_INT_ARGB); // TYPE_4BYTE_ABGR
        Graphics2D gr = result.createGraphics();
        gr.setColor(Color.red);
        gr.setComposite(AlphaComposite.SrcOver);
        gr.drawImage(image, (int) left, (int) top, finalWidth, finalHeight, 0, 0, (int) (image.getWidth() + right), (int) (image.getHeight() + bottom), null);
        gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        gr.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        ImageIO.write(result, "png", new File(resizedFileLocation));
        gr.dispose();
    }

    private double[] calculateMargins(double leftTopLat, double leftTopLng, double rightBottomLat, double rightBottomLng, int zoom) {

        int[] tileCoordinates = getTileNumber(leftTopLat, leftTopLng, zoom);
        GeoBounds leftTopBounds = boundsOfTile(tileCoordinates[0], tileCoordinates[1], zoom);
        GeoPosition topLeft = new GeoPosition(leftTopBounds.end.getLat(), leftTopBounds.start.getLng());
        tileCoordinates = getTileNumber(rightBottomLat, rightBottomLng, zoom);
        GeoBounds rightBottomBounds = boundsOfTile(tileCoordinates[0], tileCoordinates[1], zoom);

        GeoPosition bottomRight = new GeoPosition(rightBottomBounds.start.getLat(), rightBottomBounds.end.getLng());

        double topDiff = Math.abs(topLeft.getLat() - leftTopLat);
        double leftDiff = Math.abs(topLeft.getLng() - leftTopLng);

        double bottomDiff = Math.abs(bottomRight.getLat() - rightBottomLat);
        double rightDiff = Math.abs(bottomRight.getLng() - rightBottomLng);

        return new double[]{leftDiff, topDiff, rightDiff, bottomDiff};
    }

    private int[] getSizeInTiles(double leftTopLat, double leftTopLng, double rightBottomLat, double rightBottomLng, int zoom) {
        int[] startTileCoords = getTileNumber(leftTopLat, leftTopLng, zoom);
        int[] endTileCoords = getTileNumber(rightBottomLat, rightBottomLng, zoom);

        System.out.println("Width: " + (Math.abs(startTileCoords[0] - endTileCoords[0]) + 1) + " Height: "
                + (Math.abs(startTileCoords[1] - endTileCoords[1]) + 1));
        return new int[]{Math.abs(startTileCoords[0] - endTileCoords[0]) + 1, Math.abs(startTileCoords[1] - endTileCoords[1]) + 1};
    }

    private void splitImage(String imageName, String resultDir, int rows, int cols, int zoom, int startX, int startY) throws Exception {
        BufferedImage image = ImageIO.read(new FileInputStream(new File(imageName)));

        int chunkWidth = image.getWidth() / cols;
        int chunkHeight = image.getHeight() / rows;
        int w = chunkWidth * cols;
        int h = chunkHeight * rows;
        image = resizeImage(image, w, h);
        for (int x = 0; x < rows; x++) {
            File file;
            Graphics2D gr;
            BufferedImage tile;
            for (int y = 0; y < cols; y++) {
                file = new File(resultDir + File.separator + (startX) + "-" + (startY) + "-" + zoom + ".png");
                tile = new BufferedImage(DEFAULT_TILE_WIDTH, DEFAULT_TILE_HEIGHT, image.getType());
                gr = tile.createGraphics();
                BufferedImage currentTile = null;
                if (file.exists()) {
                    currentTile = ImageIO.read(file);
                }
                if (currentTile != null) {
                    gr.drawImage(currentTile, 0, 0, currentTile.getWidth(), currentTile.getHeight(), null);
                }
                gr.drawImage(getScaledImage(image, chunkWidth, chunkHeight, cols, rows, y, x), 0, 0, DEFAULT_TILE_WIDTH, DEFAULT_TILE_HEIGHT, null);
                gr.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
                gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                gr.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                ImageIO.write(tile, "png", file.exists() ? file : new File(resultDir + File.separator + (startX) + "-" + (startY) + "-" + zoom + ".png"));
                gr.dispose();
                startX++;
            }
            startY++;
        }
    }

    private BufferedImage getScaledImage(BufferedImage image, int chunkWidth, int chunkHeight, int cols, int rows, int y, int x) throws IOException {
        BufferedImage result = new BufferedImage(chunkWidth, chunkHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gr = result.createGraphics();
        gr.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        gr.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gr.drawImage(image, 0, 0, chunkWidth, chunkHeight,
                chunkWidth * y,
                chunkHeight * x,
                chunkWidth * y + chunkWidth,
                chunkHeight * x + chunkHeight,
                null);
        gr.dispose();

        return Thumbnails.of(result)
                .size(DEFAULT_TILE_WIDTH, DEFAULT_TILE_HEIGHT)
                .asBufferedImage();
    }

    private GeoBounds boundsOfTile(int x, int y, int zoom) {
        int emptyTiles = (1 << zoom);
        double longitudeSpan = 360.0 / emptyTiles;
        double longitudeMin = -180.0 + x * longitudeSpan;

        double mercatorMax = 180 - (((double) y) / emptyTiles) * 360;
        double mercatorMin = 180 - (((double) y + 1) / emptyTiles) * 360;
        double latitudeMax = toLatitude(mercatorMax);
        double latitudeMin = toLatitude(mercatorMin);

        return new GeoBounds(new GeoPosition(latitudeMin, longitudeMin), new GeoPosition(latitudeMax, longitudeMin + longitudeSpan));
    }

    public void setMapImg(File mapImg) {
        this.mapImg = mapImg;
    }

    public void setResizedMapImageLocation(String resizedMapImageLocation) {
        this.resizedMapImageLocation = resizedMapImageLocation;
    }

    public void setResultDir(String resultDir) {
        this.resultDir = resultDir;
    }

    public void setLeftTopLat(double leftTopLat) {
        this.leftTopLat = leftTopLat;
    }

    public void setLeftTopLng(double leftTopLng) {
        this.leftTopLng = leftTopLng;
    }

    public void setRightBottomLat(double rightBottomLat) {
        this.rightBottomLat = rightBottomLat;
    }

    public void setRightBottomLng(double rightBottomLng) {
        this.rightBottomLng = rightBottomLng;
    }

    //Customize tile size to get best quality in zoom Levels 16-17
    public void setZoom(int zoomLevel) {
        this.zoomLevel = zoomLevel;
        switch (zoomLevel) {
            case 16:
                DEFAULT_TILE_HEIGHT = 2048;
                DEFAULT_TILE_WIDTH = 2048;
                break;
            case 17:
                DEFAULT_TILE_HEIGHT = 1024;
                DEFAULT_TILE_WIDTH = 1024;
                break;
            default:
                DEFAULT_TILE_HEIGHT = 512;
                DEFAULT_TILE_WIDTH = 512;
                break;
        }
    }
}
