

/**
 * @author Andrei Kutsenko
 */
public class GeoBounds {

	GeoPosition start;
	GeoPosition end;

	GeoBounds(GeoPosition start, GeoPosition end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public String toString() {
		return "[" + start.toString() + " , " + end.toString() + "]";
	}

}
