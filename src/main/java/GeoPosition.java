import net.sf.json.JSONObject;

/**
 * @author Andrei Kutsenko
 */
public class GeoPosition {

	private double lat;
	private double lng;

	double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	GeoPosition(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}

	public GeoPosition(JSONObject jsonObject) {
		lat = jsonObject.getDouble("lat");
		lng = jsonObject.getDouble("lon");

	}

	@Override
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}

}
